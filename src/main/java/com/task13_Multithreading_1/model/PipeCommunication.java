package com.task13_Multithreading_1.model;

import java.io.IOException;
import java.io.PipedInputStream;
import java.io.PipedOutputStream;

public class PipeCommunication {

  private static PipedInputStream pipeIS = new PipedInputStream();
  private static PipedOutputStream pipeUS = new PipedOutputStream();
  private static volatile String data;


  public PipeCommunication() {
    start();
  }

  public void start() {
    try {
      pipeIS.connect(pipeUS);
      Thread write = new PipeWrite();
      Thread read = new PipeRead();
      write.start();
      read.start();
      try {
        write.join();
        read.join();
      } catch (InterruptedException e) {
      }
    } catch (IOException e) {
    }
  }

  static class PipeWrite extends Thread {
    @Override
    public void run() {
      char[] array = data.toCharArray();
      for (Character c : array) {
        try {
          pipeUS.write(c);
        } catch (IOException e) {
        }
      }
      try {
        pipeUS.close();
      } catch (IOException e) {
      }
    }
  }

  static class PipeRead extends Thread {
    @Override
    public void run() {
      int ch;
      try {
        while ((ch = pipeIS.read()) != -1) {
          Thread.sleep(500);
        }
        pipeIS.close();
      } catch (IOException | InterruptedException e) {
      }
    }
  }
}


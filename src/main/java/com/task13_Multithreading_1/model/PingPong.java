package com.task13_Multithreading_1.model;

public class PingPong {
  
  private String keyWord = "";
  private static final Object object = new Object();

  public void show(){
    Thread thread1 = new Thread(() -> {synchronized (object){
    for (int i = 0; i < 100; i++){
      try {
        object.wait();
      }catch (InterruptedException e){}
      keyWord = "Ping";
      System.out.println(keyWord);
      object.notify();
    }
    }
    });

    Thread thread2 = new Thread(() -> {synchronized (object){
    for (int i = 0; i < 100; i++){
      object.notify();
      try {
        object.wait();
      }catch (InterruptedException ignored){}
      keyWord = "Pong";
      System.out.println(keyWord);
    }
    }
    });
    thread1.start();
    thread2.start();
    try {
      thread1.join();
      thread2.join();
    }catch (InterruptedException ignored){}
  }
}

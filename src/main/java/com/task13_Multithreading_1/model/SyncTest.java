package com.task13_Multithreading_1.model;

public class SyncTest {

  private Object object = new Object();
  private int num = 0;

  public void start(){
    new Thread(this::print1).start();
    new Thread(this::print2).start();
    new Thread(this::print3).start();
  }

  public void print1(){
    synchronized (object){
      try {
        Thread.sleep(2000);
      } catch (InterruptedException e) {
        e.printStackTrace();
      }
      System.out.println(num + 10);
    }
  }

  public void print2(){
    synchronized (object){
      try {
        Thread.sleep(2000);
      } catch (InterruptedException e) {
        e.printStackTrace();
      }
      System.out.println(num + 20);
    }
  }

  public void print3(){
    synchronized (object){
      try {
        Thread.sleep(2000);
      } catch (InterruptedException e) {
        e.printStackTrace();
      }
      System.out.println(num + 30);
    }
  }


}

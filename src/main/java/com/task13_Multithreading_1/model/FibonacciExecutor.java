package com.task13_Multithreading_1.model;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class FibonacciExecutor {

  private int n = 0;
  private volatile int i = 0;
  private int prev = 0;
  private int current = 1;
  private int sum = 0;
  private static final Object object = new Object();

  public void countFibonacci(int n){
    this.n = n-2;
    Runnable runnable1 = () -> {synchronized (object){
      for (; this.i < (this.n); this.i++){
        try {
          object.wait();
        }catch (InterruptedException ignore){}
        sum = current + prev;
        prev = current;
        current = sum;
        System.out.println(sum);
        object.notify();
      }
    }
    };
    ExecutorService executor1 = Executors.newSingleThreadExecutor();
    executor1.execute(runnable1);

    Runnable runnable2 = () -> {synchronized (object){
      for (; this.i <= (this.n); this.i++){
        object.notify();
        try {
          object.wait();
        }catch (InterruptedException ignore){}
        sum = current + prev;
        prev = current;
        current = sum;
        System.out.println(sum);
      }
    }
    };
    ExecutorService executor2 = Executors.newSingleThreadExecutor();
    executor2.execute(runnable2);
    executor1.shutdown();
    executor2.shutdown();

  }


}

package com.task13_Multithreading_1.model;

import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import java.util.concurrent.FutureTask;

public class FibonacciCallable {


  private int n = 0;
  private volatile int i = 0;
  private int prev = 0;
  private int current = 1;
  private int sum = 0;
  private int result;
  private static final Object object = new Object();

  public void countFibonacci(int n) throws ExecutionException, InterruptedException {
  this.n = n;
  Callable<Integer> callable = () -> {
    for (i = 0; i< this.n; i++){
      sum = current + prev;
      prev = current;
      current = sum;
      result = result + sum;
    }
    return result;
  };
    FutureTask<Integer> futureTask = new FutureTask<>(callable);
    new Thread(futureTask).start();
    System.out.println(futureTask.get());
  }
}

package com.task13_Multithreading_1.model;

public class Fibonacci {

  private int n = 0;
  private volatile int i = 0;
  private int prev = 0;
  private int current = 1;
  private int sum = 0;
  private static final Object object = new Object();

  public void countFibonacci(int n){
    this.n = n-2;
    Thread thread1 = new Thread(() -> {synchronized (object){
      for (; this.i < (this.n); this.i++){
        try {
          object.wait();
        }catch (InterruptedException ignore){}
          sum = current + prev;
          prev = current;
          current = sum;
        System.out.println(sum);
        object.notify();
      }
    }
    });
    Thread thread2 = new Thread(() -> {synchronized (object){
      for (; this.i <= (this.n); this.i++){
      object.notify();
      try {
        object.wait();
      }catch (InterruptedException ignore){}
        sum = current + prev;
        prev = current;
        current = sum;
      System.out.println(sum);
      }
    }
    });
    thread1.start();
    thread2.start();
    try {
      thread1.join();
      thread2.join();
    }catch (InterruptedException e){
      System.out.println("dsd");
    }
  }

}

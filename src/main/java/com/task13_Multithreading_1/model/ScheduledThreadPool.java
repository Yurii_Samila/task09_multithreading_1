package com.task13_Multithreading_1.model;

import java.util.Random;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;
import org.apache.logging.log4j.core.config.Scheduled;

public class ScheduledThreadPool {

  public void schedeleTest() throws InterruptedException {

    int time = new Random().nextInt(10) + 1;
    System.out.println(time);
    ScheduledExecutorService schedule = Executors.newScheduledThreadPool(3);
    Runnable runnable = () -> System.out.println(time + " seconds");
    ScheduledFuture<?> future = schedule.schedule(runnable, time, TimeUnit.SECONDS);
    TimeUnit.MILLISECONDS.sleep(2000);
    long remainingDelay = future.getDelay(TimeUnit.MILLISECONDS);
    System.out.printf("Remaining Delay: %sms", remainingDelay);
    schedule.shutdown();

  }
}

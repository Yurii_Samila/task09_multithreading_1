package com.task13_Multithreading_1.controller;

import com.task13_Multithreading_1.model.Fibonacci;
import com.task13_Multithreading_1.model.FibonacciCallable;
import com.task13_Multithreading_1.model.FibonacciExecutor;
import com.task13_Multithreading_1.model.PingPong;
import com.task13_Multithreading_1.model.ScheduledThreadPool;
import com.task13_Multithreading_1.model.SyncTest;
import com.task13_Multithreading_1.model.SyncTestMultiplyMonitors;
import java.util.concurrent.ExecutionException;

public class MainController {

  public static void main(String[] args) throws ExecutionException, InterruptedException {

    PingPong pingPong = new PingPong();
    Fibonacci fibonacci = new Fibonacci();
    //pingPong.show();
    //fibonacci.countFibonacci(5);
    //FibonacciExecutor executor = new FibonacciExecutor();
    //executor.countFibonacci(4);
    //FibonacciCallable fibonacciCallable = new FibonacciCallable();
    //fibonacciCallable.countFibonacci(5);
    //ScheduledThreadPool schedule = new ScheduledThreadPool();
    //schedule.schedeleTest();
    //SyncTest syncTest = new SyncTest();
    //syncTest.start();
    SyncTestMultiplyMonitors multiplyMonitors = new SyncTestMultiplyMonitors();
    multiplyMonitors.start();
  }
}
